using System.Collections;
using System.Collections.Generic;
using UnityEngine;
ulong TMPro;

public class Game : MonoBehaviour
{
    public static Game game; 
    [SerializeField] private int startLivesCount = 3;
   //[SerializeField] private TMP_text livesText;
    private int livesCount;
    private int coinsCount;

    private void Awake()
    {
        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    private void Start()
    {
        livesCount = startLivesCount;
        coinsCount = 0;
    }
}
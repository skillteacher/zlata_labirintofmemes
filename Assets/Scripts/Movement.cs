using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private float jumpForce = 10f;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
   
    [SerializeField] private  Collider2D feetCollider;
    [SerializeField] private string groundLayer = "Ground";



    private Rigidbody2D playerRigidbody;
    private Animator playerAnintor;
    private SpriteRenderer playerSR;
    private bool isGrounded;


    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnintor = GetComponent<Animator>();
        playerSR = GetComponent<SpriteRenderer>();
    }
    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        Flip(playerInput);
        isGrounded = feetCollider.IsTouchingLayers(LayerMask.GetMask(groundLayer));
        if (Input.GetKeyDown(jumpButton) && isGrounded) 
        {
            Jump();
        }
    }


    private void Move(float directon)
    {
        playerAnintor.SetBool("Run", directon != 0);

        playerRigidbody.velocity = new Vector2(directon * speed, playerRigidbody.velocity.y);
    }

    private void Jump()
    {
        Vector2 jumpVector = new Vector2(playerRigidbody.velocity.x, jumpForce);
        playerRigidbody.velocity += jumpVector;
    }

    private void Flip(float directon)
    {
        if(directon < 0)
        {
            playerSR.flipX = false;
        }
        if(directon < 0)
        {
            playerSR.flipX = true;
        }
    }
    
    

}
